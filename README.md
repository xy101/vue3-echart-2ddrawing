# vue3-echart

基于Vue3和eCharts的图表和画板
使用eCharts绘制图表，并在图表上绘制辅助线。

## 安装依赖

```sh
npm install
```

### 在本地运行

```sh
npm run dev
```

### 项目打包

```sh
npm run build
```
